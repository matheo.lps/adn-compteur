﻿
/*
 * Project: ADN
 * Details: Afficher le nombre de lettres différentes dans un fichier ADN.
 * Author: Mathéo Lopes
 * Version: 0.1
 */

namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("C:\\Users\\MATHEO.LPS\\Desktop\\2022-2023\\Outils-du-Dev\\Projet-ADN\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();

            int na = 0; // Number of A
            int nc = 0; // Number of C
            int nt = 0; // Number of T
            int ng = 0; // Number of G

            while (line != null)
            {
                //Console.WriteLine(line);
                //Console.WriteLine(line[0]);

                for (int i = 0; i < line.Length; i++)
                {
                    //Console.WriteLine($"i={i}");
                    //Console.WriteLine(line[i]);

                    switch (line[i])
                    {
                        case 'A':
                            na++;
                            break;

                        case 'C':
                            nc++;
                            break;

                        case 'T':
                            nt++;
                            break;

                        case 'G':
                            ng++;
                            break;
                    }

                    /*if (line[i] == 'A'){na++;}
                    if (line[i] == 'C'){nc++;}
                    if (line[i] == 'T'){nt++;}
                    if (line[i] == 'G'){ng++;}*/
                }
                line = s.ReadLine();
            }
            Console.WriteLine("Done!");
            Console.WriteLine($"Nombre de 'A' = {na}");
            Console.WriteLine($"Nombre de 'C' = {nc}");
            Console.WriteLine($"Nombre de 'T' = {nt}");
            Console.WriteLine($"Nombre de 'G' = {ng}");
        }
    }
}